Running with ngrok:

- Ensure that ngrok is intalled on your machine

- Create an account at `ngrok.com`

- Install your auth token: `dashboard.ngrok.com/get-started/your-authtoken`

- Run the app on `localhost:5173/` with `npm run dev`

- Run `ngrok http 5173`

- The outout specifes the url of the ngrok server:

```
Forwarding                    https://xxx-xx-xxx-xx-xx.eu.ngrok.io -> http://localhost:5173  
```